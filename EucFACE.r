################################################################
# Function to create variable names based on input data
################################################################
varnames <- function() {
  if(is.null(input$datasets)) return()
  
  dat <- getdata()
  cols <- colnames(dat)
  names(cols) <- paste(cols, " {", sapply(dat,class), "}", sep = "")
  cols
}

################################################################
# Output controls for the Summary, Plots, and Extra tabs
# The tabs are re-used for various tools. Depending on the tool
# selected by the user the appropropriate analaysis function 
# is called.
# Naming conventions: The reactive function to be put in the
# code block above must be of the same name as the tool
# in the tools drop-down. See global.R for the current list
# of tools (and tool-names) 
################################################################

### Creating dynamic tabsets - From Alex Brown

# Generate output for the summary tab
# output$summary <- renderUI(function() {
output$summary <- renderPrint({
  if(is.null(input$datasets) || input$tool == 'dataview') return()
  
  # get the summary function for currenly selected tool and feed
  # it the output from one of the analysis reactives above
  # get-function structure is used because there may be a large
  # set of tools that will have the same output structure
  f <- get(paste("summary",input$tool,sep = '.'))
  result <- get(input$tool)()
  if(is.character(result)) {
    cat(result,"\n")
  } else {
    f(result)
  }
})

# Generate output for the plots tab
output$plots <- renderPlot({
  
  # plotting could be expensive so only done when tab is being viewed
  if(input$tool == 'dataview' || input$analysistabs != 'Plots') return()
  
  f <- get(paste("plot",input$tool,sep = '.'))
  result <- get(input$tool)()
  if(!is.character(result)) {
    f(result)
  } else {
    plot(x = 1, type = 'n', main="No variable selection made", axes = FALSE, xlab = "", ylab = "")
  }
}, width=700, height=700)