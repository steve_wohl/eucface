shinyServer(function(input, output) {
  
  # source base functions
  source('EucFACE.R', local = TRUE)
  
  # source enabled analysis tools
  flist <- sourceDirectory('tools', recursive = TRUE)
  # print(flist)
  
  # the 'grand' analysis ui-element caller
  output$ui_analysis <- renderUI({
    if(input$tool == "dataview") return()
    get(paste('ui_',input$tool, sep=""))()
  })
  
})