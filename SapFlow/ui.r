library(shiny)
library(ggplot2)
library(xtable)

shinyUI(pageWithSidebar(
  headerPanel("Sap Flow Data Viewer"),
  sidebarPanel(
    selectInput("Search", "EucFACE Sap Flow Dataset:", 
                c("HeatPulsers" = "FACE_R[1-6]_B[2-3]_sap_Heat",
                  "ThermInfo" = "FACE_R[1-6]_B[2-3]_sap_Therm"),
                selected = "HeatPulsers"),
    
    dateRangeInput("daterange", "Date range:",
                   start = Sys.Date()-10,
                   end = Sys.Date()),
    submitButton(text = "Apply Changes"),
    uiOutput("ui_visualize")
  ),
  
  mainPanel(
    tabsetPanel(
      tabPanel("Ring_1", plotOutput("Ring_1", width = "100%", height = "640px")),
      tabPanel("Ring_2", plotOutput("Ring_2", width = "100%", height = "640px")),
      tabPanel("Ring_3", plotOutput("Ring_3", width = "100%", height = "640px")),
      tabPanel("Ring_4", plotOutput("Ring_4", width = "100%", height = "640px")),
      tabPanel("Ring_5", plotOutput("Ring_5", width = "100%", height = "640px")),
      tabPanel("Ring_6", plotOutput("Ring_6", width = "100%", height = "640px"))
    )
  )
  ))