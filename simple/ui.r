library(shiny)
library(ggplot2)
library(xtable)

shinyUI(pageWithSidebar(
  headerPanel("Simple TOA5 Return"),
  sidebarPanel(
    textInput("Search", "HIEv TOA5 file exploration:", "Use Regular Expressions"),
    
    dateRangeInput("daterange", "Date range:",
                   start = Sys.Date()-10,
                   end = Sys.Date()+10),
    submitButton(text = "Apply Changes"),
    uiOutput("ui_visualize")
  ),
  
  mainPanel(
    tabsetPanel(
      tabPanel("Summary", verbatimTextOutput("summary")),
      tabPanel("Plot", plotOutput("plot", width = "100%", height = "640px")),  
      tabPanel("Table", tableOutput("table"))
      )
    )
  ))