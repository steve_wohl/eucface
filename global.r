#Global File for the EucFACE R App
options(repos = c("http://cran.rstudio.com/"))
libs <- c("shiny", "shinyIncubator", "tools", "ggplot2","reshape2", "plyr", "markdown", "lubridate", "HIEv")
available <- suppressWarnings(suppressPackageStartupMessages(sapply(libs, require, character.only=TRUE)))
inst.libs <- libs[available == FALSE]
if(length(inst.libs) != 0) {
  install.packages(inst.libs, dependencies = TRUE)
  suppressWarnings(suppressPackageStartupMessages(sapply(inst.libs, require, character.only=TRUE)))
}

output$columns <- renderUI({
  cols <- varnames()
  selectInput("columns", "Select columns to show:", choices  = as.list(cols), selected = names(cols), multiple = TRUE)
})

setToken()



